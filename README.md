# Most Popular RSS #

The Most Popular RSS module is dependent on another active module called
[Most Popular](http://drupal.org/project/mostpopular).  The Most Popular
module finds the pages on a Drupal site that users most commonly view, comment
on, print or otherwise interact with, using a number of different third-party
services.  The Most Popular module comes packaged with support for Google
Analytics, Disqus, AddThis.com and Drupal's own internal statistics, but more
services can be added using hooks.

The Most Popular RSS module, in turn, creates RSS feeds based on each of the
services and intervals configured for the Most Popular module.

## URL format ##

### Drupal 6: 
/mostpopular/items/%service/%interval/rss.xml

### Drupal 7: 
/mostpopular/%block/%service/%interval/rss.xml

## Supporting Organization:
[New Signature](https://www.drupal.org/node/1123366)
[http://www.newsignature.com](http://www.newsignature.com)
